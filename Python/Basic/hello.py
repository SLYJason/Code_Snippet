#!/usr/bin/env python
# -*- coding: utf-8 -*-

# escape character
# print r'\t\n\t\n\'\"\t\n'

# multiline
# print '''one line
# two line
# three line'''

# Encode & Decode
# print u'中文'
# print u'中'
# 英文字符转换后表示的UTF-8的值和Unicode值相等（但占用的存储空间不同），而中文字符转换后1个Unicode字符将变为3个UTF-8字符
# '\xe4\xb8\xad\xe6\x96\x87'.decode('utf-8')
# print '\xe4\xb8\xad\xe6\x96\x87'.decode('utf-8')

# Format output
# print '%02d-%02d' % (3, 1)
# print 'growth rate: %d %%' % 7

# Define tuple
# Define only one element tuple
# t = (1,)
# print t

age = 20
if age >= 18:
    print 'your age is', age 
    print 'adult'
else:
	print 'jha'